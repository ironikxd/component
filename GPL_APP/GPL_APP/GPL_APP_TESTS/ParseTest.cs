﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using GPL_APP;
using System.Drawing;
using System.Windows.Forms;

namespace GPL_APP_TESTS
{
    [TestClass]
    public class ParseTest
    {

        [TestMethod]
        public void TestingDrawFromLineWithInvalidParameter()
        {

            PictureBox display = new PictureBox();
            Parse parse = new Parse(new Canvas(Graphics.FromImage(new Bitmap(500,400)), display));
            
            

            try
            {
                parse.DrawFromLine("triangle x,y", 0);
            }
            catch (InvalidParameterException e)
            {
                // Assert
                StringAssert.Contains(e.Message, "Invalid Parameter given for the command");
                return;
            }

            Assert.Fail("The expected exception was not thrown.");
        }

        [TestMethod]
        public void TestingDrawFromLineWithInvalidCommand()
        {
            PictureBox display = new PictureBox();
            Parse parse = new Parse(new Canvas(Graphics.FromImage(new Bitmap(500, 400)), display));



            try
            {
                parse.DrawFromLine("test", 0);
            }
            catch (InvalidCommandException e)
            {
                Console.WriteLine(e);
                // Assert
                StringAssert.Contains(e.Message, "Invalid command");
                return;
            }

            Assert.Fail("The expected exception was not thrown.");
        }
    }
}
